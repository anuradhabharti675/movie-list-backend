const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
const logger = require("../../logger");

dotenv.config();

// eslint-disable-next-line consistent-return
module.exports = function (req, res, next) {
  const token = req.header("auth-token");
  if (!token) return res.status(401).send("Access Denied");
  try {
    const verified = jwt.verify(token, process.env.TOKEN_SECRET);
    req.user = verified;
    next();
  } catch (err) {
    logger.log("error", err.stack);
    res.status(400).end("Invalid Token");
  }
};
