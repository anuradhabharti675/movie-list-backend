const { Pool } = require("pg");

const {
  fetchNowPlayingMovies,
  fetchGenres,
  fetchPopularMovies,
  fetchTopRatedMovies,
  fetchUpcomingMovies,
} = require("../movieService");

const pool = new Pool({
  user: "postgres",
  password: "anu@675",
  host: "localhost",
  port: 5432,
  database: "movies_db",
});
pool.on("error", (err, client) => {
  console.error("Unexpected error on idle client", err.message);
  process.exit(-1);
});
(async () => {
  const client = await pool.connect();
  try {
    // insert genres
    // const genres = await fetchGenres();
    // genres.forEach(async (genre) => {
    //   try {
    //     await client.query(`INSERT INTO Genres
    //       (genre_id,genre)
    //       VALUES(${genre.id},'${genre.name}');`);
    //   } catch (err) {
    //     console.log(err.stack);
    //   }
    // });

    // insert now playing movies
    const nowPlayingMovies = await fetchNowPlayingMovies();
    nowPlayingMovies.forEach(async (movie) => {
      try {
        console.log(movie, "movie of now playing");
        await client.query(
          `INSERT INTO Now_playing_movies
             (movie_id,
              title,
              description,
              rating,
              release_date,
              poster_path
            )
           VALUES($1,$2,$3,$4,$5,$6);`,
          [
            movie.id,
            movie.title,
            movie.overview,
            movie.vote_average,
            movie.release_date,
            movie.poster_path,
          ]
        );
        movie.genre_ids.forEach(async (id) => {
          try {
            await client.query(`INSERT INTO Genre_nowplaying_movie
          (movie_id,genre_id)
          VALUES(${movie.id},${id})`);
          } catch (err) {
            console.log(err.stack);
          }
        });
      } catch (err) {
        console.log(err);
      }
    });
    const upcomingMovies = await fetchUpcomingMovies();
    upcomingMovies.forEach(async (movie) => {
      try {
        console.log(movie, "movie of now playing");
        await client.query(
          `INSERT INTO upcoming_movies
             (movie_id,
              title,
              description,
              rating,
              release_date,
              poster_path
            )
           VALUES($1,$2,$3,$4,$5,$6);`,
          [
            movie.id,
            movie.title,
            movie.overview,
            movie.vote_average,
            movie.release_date,
            movie.poster_path,
          ]
        );
        movie.genre_ids.forEach(async (id) => {
          try {
            await client.query(`INSERT INTO Genre_upcoming_movie
          (movie_id,genre_id)
          VALUES(${movie.id},${id})`);
          } catch (err) {
            console.log(err.stack);
          }
        });
      } catch (err) {
        console.log(err);
      }
    });
    // insert  popular movies
    // const popularMovies = await fetchPopularMovies();
    // // console.log(nowPlayingMovies, "data");
    // popularMovies.forEach(async (movie) => {
    //   try {
    //     console.log(movie);
    //     await client.query(
    //       `INSERT INTO Popular_movies
    //          (movie_id,
    //           title,
    //           description,
    //           rating,
    //           release_date,
    //           poster_path
    //         )
    //        VALUES($1,$2,$3,$4,$5,$6);`,
    //       [
    //         movie.id,
    //         movie.title,
    //         movie.overview,
    //         movie.vote_average,
    //         movie.release_date,
    //         movie.poster_path,
    //       ]
    //     );
    //     movie.genre_ids.forEach(async (id) => {
    //       // console.log(id, movie.id);
    //       try {
    //         await client.query(`INSERT INTO Genre_popular_movie
    //       (movie_id,genre_id)
    //       VALUES(${movie.id},${id})`);
    //       } catch (err) {
    //         console.log(err.stack);
    //       }
    //     });
    //   } catch (err) {
    //     console.log(err);
    //   }
    // });

    // insert top rated movies
    // const topRatedMovies = await fetchTopRatedMovies();
    // // console.log(nowPlayingMovies, "data");
    // topRatedMovies.forEach(async (movie) => {
    //   try {
    //     console.log(movie);
    //     await client.query(
    //       `INSERT INTO Toprated_movies
    //          (movie_id,
    //           title,
    //           description,
    //           rating,
    //           release_date,
    //           poster_path
    //         )
    //        VALUES($1,$2,$3,$4,$5,$6);`,
    //       [
    //         movie.id,
    //         movie.title,
    //         movie.overview,
    //         movie.vote_average,
    //         movie.release_date,
    //         movie.poster_path,
    //       ]
    //     );
    //     movie.genre_ids.forEach(async (id) => {
    //       // console.log(id, movie.id);
    //       try {
    //         await client.query(`INSERT INTO Genre_toprated_movie
    //       (movie_id,genre_id)
    //       VALUES(${movie.id},${id})`);
    //       } catch (err) {
    //         console.log(err.stack);
    //       }
    //     });
    //   } catch (err) {
    //     console.log(err);
    //   }
    // });
  } finally {
    client.release();
  }
})().catch((err) => console.log(err.stack));
