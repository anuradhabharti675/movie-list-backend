const { Pool } = require("pg");

const pool = new Pool({
  user: "postgres",
  password: "anu@675",
  host: "localhost",
  port: 5432,
  database: "movies_db",
});
pool.on("error", (err) => {
  console.error("Unexpected error on idle client", err.message);
  process.exit(-1);
});
(async () => {
  const client = await pool.connect();
  try {
    await client.query(`CREATE TABLE IF NOT EXISTS Actor
(actor_id serial PRIMARY KEY, actor_name  varchar(150),actor_img varchar(500),gender varchar(10));`);
  } finally {
    client.release();
  }
})().catch((err) => console.log(err.stack));
