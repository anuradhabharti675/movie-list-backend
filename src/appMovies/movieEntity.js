const { Pool } = require("pg");

const pool = new Pool({
  user: "postgres",
  password: "anu@675",
  host: "localhost",
  port: 5432,
  database: "movies_db",
});
pool.on("error", (err) => {
  console.error("Unexpected error on idle client", err.message);
  process.exit(-1);
});
(async () => {
  const client = await pool.connect();
  try {
    await client.query(`CREATE TABLE IF NOT EXISTS Genres
    (genre_id integer PRIMARY KEY,
      genre varchar(100)
    );`);
    await client.query(`CREATE TABLE IF NOT EXISTS User_Detail
       ( id serial PRIMARY KEY,
         name varchar(100) NOT NULL,
         email varchar(500) NOT NULL ,
         password varchar(2000) NOT NULL,
         date  DATE NOT NULL DEFAULT CURRENT_DATE,
         unique(email)
       );`);
    await client.query(`CREATE TABLE IF NOT EXISTS Genres
       (genre_id integer PRIMARY KEY,
         genre varchar(100)
       );`);
    await client.query(`CREATE TABLE IF NOT EXISTS Actor
         (actor_id serial PRIMARY KEY, actor_name  varchar(150),actor_img varchar(500),gender varchar(10));`);
    await client.query(`CREATE TABLE IF NOT EXISTS Now_playing_Movies
         (movie_id integer PRIMARY KEY,
          title varchar(250),
          description varchar(1500),
          rating float,
          release_date varchar(20),
          poster_path varchar(200)
          );`);
    await client.query(`CREATE TABLE IF NOT EXISTS Genre_nowplaying_movie
          ( gen_mv_id serial PRIMARY KEY,
            movie_id integer ,
            genre_id integer ,
            FOREIGN KEY (movie_id) REFERENCES Now_playing_movies(movie_id),
            FOREIGN KEY (genre_id) REFERENCES Genres(genre_id)
          );`);
    await client.query(`CREATE TABLE IF NOT EXISTS Upcoming_Movies
          (movie_id integer PRIMARY KEY,
           title varchar(250),
           description varchar(1500),
           rating float,
           release_date varchar(20),
           poster_path varchar(200)
           );`);
    await client.query(`CREATE TABLE IF NOT EXISTS Genre_upcoming_movie
           ( gen_mv_id serial PRIMARY KEY,
             movie_id integer ,
             genre_id integer ,
             FOREIGN KEY (movie_id) REFERENCES upcoming_movies(movie_id),
             FOREIGN KEY (genre_id) REFERENCES Genres(genre_id)
           );`);
    await client.query(`CREATE TABLE IF NOT EXISTS Popular_movies
      (movie_id integer PRIMARY KEY,
       title varchar(250),
       description varchar(1500),
       rating float,
       release_date varchar(20),
       poster_path varchar(200)
       );`);
    await client.query(`CREATE TABLE IF NOT EXISTS Genre_popular_movie
       ( gen_mv_id serial PRIMARY KEY,
         movie_id integer ,
         genre_id integer ,
         FOREIGN KEY (movie_id) REFERENCES Popular_movies(movie_id),
         FOREIGN KEY (genre_id) REFERENCES Genres(genre_id)
       );`);
    await client.query(`CREATE TABLE IF NOT EXISTS Toprated_movies
       (movie_id integer PRIMARY KEY,
        title varchar(250),
        description varchar(1500),
        rating float,
        release_date varchar(20),
        poster_path varchar(200)
        );`);
    await client.query(`CREATE TABLE IF NOT EXISTS Genre_toprated_movie
        ( gen_mv_id serial PRIMARY KEY,
          movie_id integer ,
          genre_id integer ,
          FOREIGN KEY (movie_id) REFERENCES Toprated_movies(movie_id),
          FOREIGN KEY (genre_id) REFERENCES Genres(genre_id)
        );`);
    await client.query(`CREATE TABLE IF NOT EXISTS Watch_list
    (watchlist64 integer UNIQUE );`);
  } finally {
    client.release();
  }
})().catch((err) => console.log(err.stack));
