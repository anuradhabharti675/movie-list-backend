const { Pool } = require("pg");
const logger = require("../../logger");

const pool = new Pool({
  user: "postgres",
  password: "anu@675",
  host: "localhost",
  port: 5432,
  database: "movies_db",
});

pool.on("error", (err) => {
  console.error("Unexpected error on idle client", err.message);
  process.exit(-1);
});
pool.connect();

const giveNowPlayingMovies = async (page) => {
  // eslint-disable-next-line no-return-await
  try {
    const nowPlayingMovies = await pool.query(
      `SELECT *
    FROM now_playing_movies
    ORDER BY release_date DESC`
    );
    if (page === "1") return nowPlayingMovies.rows.slice(0, 20);
    if (page === "2") return nowPlayingMovies.rows.slice(20, 40);
    if (page === "3") return nowPlayingMovies.rows.slice(40, 60);
    if (page === "4") return nowPlayingMovies.rows.slice(60, 80);
    if (page === "5") return nowPlayingMovies.rows.slice(80, 100);
    return "Invalid request";
  } catch (err) {
    logger.log("error", err.stack);
    return 0;
  }
};
const giveUpcomingMovies = async (page) => {
  // eslint-disable-next-line no-return-await
  try {
    const upcomingMovies = await pool.query(
      `SELECT *
    FROM upcoming_movies
    ORDER BY release_date DESC`
    );
    if (page === "1") return upcomingMovies.rows.slice(0, 20);
    if (page === "2") return upcomingMovies.rows.slice(20, 40);
    if (page === "3") return upcomingMovies.rows.slice(40, 60);
    if (page === "4") return upcomingMovies.rows.slice(60, 80);
    if (page === "5") return upcomingMovies.rows.slice(80, 100);
    return "Invalid request";
  } catch (err) {
    logger.log("error", err.stack);
    return 0;
  }
};

const giveTopRatedMovies = async (page) => {
  // eslint-disable-next-line no-return-await
  try {
    const topratedMovies = await pool.query(
      `SELECT *
    FROM toprated_movies
    ORDER BY release_date DESC`
    );
    if (page === "1") return topratedMovies.rows.slice(0, 20);
    if (page === "2") return topratedMovies.rows.slice(20, 40);
    if (page === "3") return topratedMovies.rows.slice(40, 60);
    if (page === "4") return topratedMovies.rows.slice(60, 80);
    if (page === "5") return topratedMovies.rows.slice(80, 100);
    return "Invalid request";
  } catch (err) {
    logger.log("error", err.stack);
    return 0;
  }
};

const givePopularMovies = async (page) => {
  // eslint-disable-next-line no-return-await
  try {
    const popularMovies = await pool.query(
      `SELECT *
    FROM popular_movies
    ORDER BY release_date DESC`
    );
    if (page === "1") return popularMovies.rows.slice(0, 20);
    if (page === "2") return popularMovies.rows.slice(20, 40);
    if (page === "3") return popularMovies.rows.slice(40, 60);
    if (page === "4") return popularMovies.rows.slice(60, 80);
    if (page === "5") return popularMovies.rows.slice(80, 100);
    return "Invalid request";
  } catch (err) {
    logger.log("error", err.stack);
    return 0;
  }
};
const giveGenres = async () => {
  try {
    return await pool.query(
      `SELECT *
  FROM genres`
    );
  } catch (err) {
    logger.log("error", err.stack);
    return 0;
  }
};
const saveUserDetail = async (user) => {
  try {
    const result = await pool.query(
      `INSERT INTO user_detail 
        (name,email,password)
        VALUES
        ('${user.name}','${user.email}','${user.password}')`
    );
    return result;
  } catch (err) {
    logger.log("error", err.stack);
    return 0;
  }
};
const findUser = async (email) => {
  try {
    const result = await pool.query(
      ` SELECT *
        FROM user_detail 
        WHERE email='${email}'
        `
    );
    return result.rows;
  } catch (err) {
    logger.log("error", err.stack);
    return 0;
  }
};
const createWatchlist = async (id) => {
  try {
    const result = await pool.query(
      `ALTER TABLE watch_list
      ADD watchlist${id} integer 
      UNIQUE;`
    );
    return result;
  } catch (err) {
    logger.log("error", err.stack);
    return 0;
  }
};
const giveWatchlistIds = async (id) => {
  try {
    const movieIds = await pool.query(`
    SELECT watchlist${id}
    FROM watch_list
    `);
    return movieIds.rows;
  } catch (err) {
    logger.log("error", err.stack);
    return 0;
  }
};
const giveWatchlistMovies = async (id) => {
  try {
    let movie = await pool.query(
      `SELECT * FROM popular_movies WHERE movie_id='${id}'`
    );
    if (movie.rows.length > 0) return movie.rows[0];
    movie = await pool.query(
      `SELECT * FROM now_playing_movies WHERE movie_id='${id}'`
    );
    if (movie.rows.length > 0) return movie.rows[0];
    movie = await pool.query(
      `SELECT * FROM upcoming_movies WHERE movie_id='${id}'`
    );
    return movie.rows[0];
  } catch (err) {
    logger.log("error", err.stack);
    return 0;
  }
};
const addMovieInWatchlist = async (id, movieId) => {
  try {
    const result = await pool.query(`
    INSERT INTO watch_list
    (watchlist${id})
    VALUES 
    (${movieId})
    `);
    return result;
  } catch (err) {
    logger.log("error", err.stack);
    return err.stack;
  }
};
const removeMovieInWatchlist = async (id, movieId) => {
  try {
    const result = await pool.query(`
    UPDATE watch_list
    SET watchlist${id}=NULL
    WHERE watchlist${id}=${movieId}
    `);
    return result;
  } catch (err) {
    logger.log("error", err.stack);
    return 0;
  }
};
const giveSearchMovies = async (title) => {
  try {
    let result = [];
    const firstResult = await pool.query(
      `SELECT * FROM popular_movies WHERE title LIKE '${title}%'`
    );
    const secondResult = await pool.query(
      `SELECT * FROM now_playing_movies WHERE title LIKE '${title}%'`
    );
    const thirdResult = await pool.query(
      `SELECT * FROM upcoming_movies WHERE title LIKE '${title}%'`
    );
    if (firstResult) result = [...firstResult.rows];
    if (secondResult) result = [...result, ...secondResult.rows];
    if (thirdResult) result = [...result, ...thirdResult.rows];
    return result;
  } catch (err) {
    logger.log("error", err.stack);
    return 0;
  }
};
const giveMovie = async (id) => {
  try {
    const firstResult = await pool.query(
      `SELECT movie.movie_id, movie.title, movie.poster_path, movie.release_date,genres.genre,movie.description
      FROM now_playing_movies as movie
      JOIN genre_nowplaying_movie ON movie.movie_id = genre_nowplaying_movie.movie_id
      JOIN genres ON genre_nowplaying_movie.genre_id = genres.genre_id
      WHERE movie.movie_id=${id}`
    );
    if (firstResult.rows.length) return firstResult.rows;
    const secondResult = await pool.query(
      `SELECT movie.movie_id, movie.title, movie.poster_path, movie.release_date,genres.genre,movie.description
      FROM upcoming_movies as movie
      JOIN genre_upcoming_movie ON movie.movie_id = genre_upcoming_movie.movie_id
      JOIN genres ON genre_upcoming_movie.genre_id = genres.genre_id
      WHERE movie.movie_id=${id}`
    );
    if (secondResult.rows.length) return secondResult.rows;
    const thirdResult = await pool.query(
      `SELECT movie.movie_id, movie.title, movie.poster_path, movie.release_date,genres.genre,movie.description
      FROM popular_movies as movie
      JOIN genre_popular_movie ON movie.movie_id = genre_popular_movie.movie_id
      JOIN genres ON genre_popular_movie.genre_id = genres.genre_id
      WHERE movie.movie_id=${id}`
    );
    if (thirdResult) return thirdResult.rows;
    return "No data found";
  } catch (err) {
    logger.log("error", err.stack);
    return 0;
  }
};
module.exports = {
  giveNowPlayingMovies,
  givePopularMovies,
  giveTopRatedMovies,
  giveUpcomingMovies,
  saveUserDetail,
  findUser,
  giveGenres,
  createWatchlist,
  giveWatchlistMovies,
  giveWatchlistIds,
  addMovieInWatchlist,
  removeMovieInWatchlist,
  giveSearchMovies,
  giveMovie,
};
