const logger = require("../../logger");

const logUserAddress = (req, res, next) => {
  logger.log("info", `ip address is :${req.ip}`);
  next();
};

module.exports = {
  logUserAddress,
};
