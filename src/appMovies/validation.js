const Joi = require("@hapi/joi");

const registerSchema = {
  name: Joi.string().min(3).required(),
  email: Joi.string().min(6).required().email(),
  password: Joi.string().min(6).required(),
};

const loginSchema = {
  email: Joi.required(),
  password: Joi.required(),
};

module.exports = {
  loginSchema,
  registerSchema,
};
