const fetch = require("node-fetch");
const dotenv = require("dotenv");

dotenv.config();

const fetchNowPlayingMovies = async () => {
  const result = await fetch(
    `https://api.themoviedb.org/3/movie/now_playing?api_key=${process.env.API_KEY}&language=en-US&page=5`
  )
    .then((response) => response.json())
    .then((data) => {
      console.log(data.results);
      return data.results;
    })
    .catch((err) => console.log(err));
  return result;
};

const fetchUpcomingMovies = async () => {
  const result = await fetch(
    `https://api.themoviedb.org/3/movie/upcoming?api_key=${process.env.API_KEY}&language=en-US&page=5`
  )
    .then((response) => response.json())
    .then((data) => {
      console.log(data.results);
      return data.results;
    })
    .catch((err) => console.log(err));
  return result;
};
const fetchPopularMovies = async () => {
  const result = await fetch(
    `https://api.themoviedb.org/3/movie/popular?api_key=${process.env.API_KEY}&language=en-US&page=5`
  )
    .then((response) => response.json())
    .then((data) => {
      // console.log(data.results);
      return data.results;
    })
    .catch((err) => console.log(err));
  return result;
};
const fetchTopRatedMovies = async () => {
  const result = await fetch(
    `https://api.themoviedb.org/3/movie/top_rated?api_key=${process.env.API_KEY}&language=en-US&page=5`
  )
    .then((response) => response.json())
    .then((data) => {
      // console.log(data.results);
      return data.results;
    })
    .catch((err) => console.log(err));
  return result;
};

const fetchGenres = async () => {
  const result = await fetch(
    `https://api.themoviedb.org/3/genre/movie/list?api_key=${process.env.API_KEY}&language=en-US`
  )
    .then((response) => response.json())
    .then((data) => {
      console.log(data.genres);
      return data.genres;
    })
    .catch((err) => console.log(err));
  return result;
};

// https://api.themoviedb.org/3/genre/movie/list?api_key=46cb0826bc9d924445bc90903e183ebf&language=en-US
module.exports = {
  fetchNowPlayingMovies,
  fetchGenres,
  fetchPopularMovies,
  fetchTopRatedMovies,
  fetchUpcomingMovies,
};

// const { fetchNowPlayingMovies,

// } = require('./ipl.js');
// img https://image.tmdb.org/t/p/w300/
