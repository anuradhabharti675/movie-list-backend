const router = require("express").Router();
const dotenv = require("dotenv");
const Joi = require("@hapi/joi");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const lodash = require("lodash");
const verify = require("../appMovies/varifyToken");
const logger = require("../../logger");
const { registerSchema, loginSchema } = require("../appMovies/validation");
const {
  saveUserDetail,
  findUser,
  giveNowPlayingMovies,
  givePopularMovies,
  giveUpcomingMovies,
  giveTopRatedMovies,
  giveGenres,
  createWatchlist,
  giveWatchlistMovies,
  addMovieInWatchlist,
  removeMovieInWatchlist,
  giveWatchlistIds,
  giveSearchMovies,
  giveMovie,
} = require("../appMovies/query");

dotenv.config();

router.get("/movies/now_playing/:page", async (req, res) => {
  try {
    const nowPlayingMovies = await giveNowPlayingMovies(req.params.page);
    if (nowPlayingMovies === "Invalid request")
      res.status(400).send(nowPlayingMovies);
    res.status(200).json(nowPlayingMovies);
  } catch (err) {
    res.json(err).status(400);
    logger.log("error", err.stack);
  }
});

router.get("/movies/popular/:page", async (req, res) => {
  try {
    const popularMovies = await givePopularMovies(req.params.page);
    if (popularMovies === "Invalid request")
      res.status(400).send(popularMovies);
    res.status(200).json(popularMovies);
  } catch (err) {
    res.json(err).status(400);
    logger.log("error", err.stack);
  }
});

router.get("/movies/upcoming/:page", async (req, res) => {
  try {
    const upcomingMovies = await giveUpcomingMovies(req.params.page);
    if (upcomingMovies === "Invalid request")
      res.status(400).send(upcomingMovies);
    res.status(200).json(upcomingMovies);
  } catch (err) {
    logger.log("error", err.stack);
  }
});
router.get("/movies/top_rated/:page", async (req, res) => {
  try {
    const topratedMovies = await giveTopRatedMovies(req.params.page);
    if (topratedMovies === "Invalid request")
      res.status(400).send(topratedMovies);
    res.status(200).json(topratedMovies);
  } catch (err) {
    res.json(err).status(400);
    logger.log("error", err.stack);
  }
});

router.get("/genres", async (req, res) => {
  try {
    const genres = await giveGenres();
    res.status(200).json(genres);
  } catch (err) {
    res.json(err).status(400);
    logger.log("error", err.stack);
  }
});

router.post("/register", async (req, res) => {
  try {
    // validate data
    const { error } = Joi.validate(req.body, registerSchema);
    if (error) {
      res.status(200).json(error.details[0].message);
      return;
    }
    // hash passwords
    const salt = await bcrypt.genSaltSync(10);
    const hashPassword = await bcrypt.hashSync(req.body.password, salt);

    const user = {
      name: req.body.name,
      email: req.body.email,
      password: hashPassword,
    };
    // checking if the user is already in the database
    const userDetail = await saveUserDetail(user);
    if (userDetail === undefined) {
      res.status(200).json("Email already exist");
      return;
    }
    // find inserted user details
    const userInserted = await findUser(user.email);
    const watchlist = await createWatchlist(Number(userInserted[0].id));
    res.status(200).json(userInserted[0]);
  } catch (err) {
    res.end(err).status(400);
    logger.log("error", err.stack);
  }
});

router.post("/login", async (req, res) => {
  try {
    // validate data
    const { error } = Joi.validate(req.body, loginSchema);
    if (error) {
      res.status(200).json(error.details[0].message);
      return;
    }
    // checking if the email exits
    const user = await findUser(req.body.email);
    if (user.length === 0) {
      res.status(200).json("user not exist!");
      return;
    }
    // checking password correct or not
    const validPass = await bcrypt.compareSync(
      req.body.password,
      user[0].password
    );
    if (!validPass) {
      res.json("Invaild password!");
      return;
    }
    // eslint-disable-next-line no-underscore-dangle
    const token = jwt.sign({ id: user[0].id }, process.env.TOKEN_SECRET);
    res
      .header("auth-token", token)
      .json({ token: token, user: user[0] })
      .status(200);
  } catch (err) {
    res.end(err.stack).status(400);
    logger.log("error", err.stack);
  }
});

router.post("/watchlist/add_movie", verify, async (req, res) => {
  try {
    const watchlist = await addMovieInWatchlist(req.body.id, req.body.movie_id);
    if (watchlist.error)
      res.json("Already added movie in watchlist!").status(500);
    res.json("added movie in watchlist!").status(200);
  } catch (err) {
    res.end(err).status(400);
    logger.log("error", err.stack);
  }
});

router.post("/watchlist/remove_movie", verify, async (req, res) => {
  try {
    const watchlist = await removeMovieInWatchlist(
      req.body.id,
      req.body.movie_id
    );
    if (watchlist.error) {
      res.json("already removed !").status(500);
      return;
    }
    res.json("removed movie from watchlist!").status(200);
    return;
  } catch (err) {
    res.end(err).status(400);
    logger.log("error", err.stack);
  }
});
router.get("/watchlist/:id", verify, async (req, res) => {
  try {
    const watchlistIds = await giveWatchlistIds(Number(req.params.id));
    const objId = `watchlist${req.params.id}`;
    Promise.all(
      watchlistIds
        .filter((id) => id[objId] !== null)
        .map(async (id) => {
          const data = await giveWatchlistMovies(id[objId]);
          return data;
        })
    )
      .then((values) => {
        res.json(values).status(200);
      })
      .catch((err) => {
        res.end(err);
      });
  } catch (err) {
    res.end(err).status(400);
    logger.log("error", err.stack);
  }
});

router.get("/search/:title", async (req, res) => {
  try {
    const userName = lodash.startCase(
      lodash.toLower(lodash.replace(req.params.title, " ", " "))
    );
    const searchMovies = await giveSearchMovies(userName);
    if (searchMovies.length === 0) res.json("Not Found!").status(200);
    res.json(searchMovies).status(200);
  } catch (err) {
    res.end(err.stack).status(400);
    logger.log("error", err.stack);
  }
});

router.get("/movie/:id", async (req, res) => {
  try {
    const movie = await giveMovie(req.params.id);
    const genre = movie.map((m) => m.genre);
    res
      .json({
        movie_id: movie[0].movie_id,
        title: movie[0].title,
        poster_path: movie[0].poster_path,
        release_date: movie[0].release_date,
        genres: genre,
        description: movie[0].description,
      })
      .status(200);
  } catch (err) {
    res.json(err).status(400);
    logger.log("error", err.stack);
  }
});
module.exports = router;
