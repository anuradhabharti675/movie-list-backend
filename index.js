const express = require("express");
const dotenv = require("dotenv");
const logger = require("./logger");
const { logUserAddress } = require("./src/appMovies/middleware");

dotenv.config();
const port = 5000;
const app = express();

// log client ip address
app.use(logUserAddress);
app.use(require("body-parser").json());
app.use("/movies-app", require("./src/movieApi/MovieController"));

// error handler
app.use((req, res, next) => {
  const error = new Error("Not found!");
  error.status = 404;
  next(error);
});
app.use((error, req, res) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message,
    },
  });
  logger.log("error", error.message);
});

app.listen(port);
